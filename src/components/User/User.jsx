import React from 'react'
import { useParams } from 'react-router-dom'

const User = () => {
    const { id } = useParams()

  return (
    <div 
    className='text-center my-10 text-3xl p-4 bg-slate-700 w-fit mx-auto rounded-lg text-white'
    >
        User : <span
        className='text-orange-700 underline'
        >
            { id }
        </span> 
    </div>
  )
}

export default User