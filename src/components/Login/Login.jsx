import React from "react";
import { Link } from "react-router-dom";

const Login = () => {
  return (
    <div class="min-h-screen flex items-center justify-center w-full">
      <div class="bg-white shadow-md rounded-lg px-8 py-6 max-w-md">
        <h1 class="text-2xl font-bold text-center mb-4">
          Welcome Back!
        </h1>
        <form action="#">
          <div class="mb-4">
            <label
              for="email"
              class="block text-sm font-medium text-gray-700 mb-2"
            >
              Email Address
            </label>
            <input
              type="email"
              id="email"
              class="shadow-sm rounded-md w-full px-3 py-2 border border-gray-300 focus:outline-none focus:ring-orange-500 focus:border-orring-orange-500"
              placeholder="your@email.com"
              required
            />
          </div>
          <div class="mb-4">
            <label
              for="password"
              class="block text-sm font-medium text-gray-700 mb-2"
            >
              Password
            </label>
            <input
              type="password"
              id="password"
              class="shadow-sm rounded-md w-full px-3 py-2 border border-gray-300 focus:outline-none focus:ring-orange-500 focus:border-orring-orange-500"
              placeholder="Enter your password"
              required
            />
            <a
              href="#"
              class="text-xs text-gray-600 hover:text-orring-orange-500 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-orange-500"
            >
              Forgot Password?
            </a>
          </div>
          <div class="flex items-center justify-between mb-4">
            <div class="flex items-center">
              <input
                type="checkbox"
                id="remember"
                class="h-4 w-4 rounded border-gray-300 text-orange-600 focus:ring-orange-500 focus:outline-none"
                checked
              />
              <label
                for="remember"
                class="ml-2 block text-sm text-gray-900 dark:text-gray-300"
              >
                Remember me
              </label>
            </div>
            <Link
              to="/signup"
              class="text-xs text-orring-orange-500 hover:text-orange-700 focus:outline-none focus:ring-2 focus:ring-offset-2 hover:underline focus:ring-orange-500"
            >
              Create Account
            </Link>
          </div>
          <button
            type="submit"
            class="w-full flex justify-center py-2 px-4 border border-transparent rounded-md shadow-sm text-sm font-medium text-white bg-orange-600 hover:bg-orange-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-orange-500"
          >
            Login
          </button>
        </form>
      </div>
    </div>
  );
};

export default Login;