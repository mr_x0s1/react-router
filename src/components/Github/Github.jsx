import React, { useEffect, useState } from "react";
import "./style.css"
import { useLoaderData } from "react-router-dom";

const Github = () => {

    const data = useLoaderData()

    /*
    const [data, setData] = useState({})

    useEffect( () => {
        fetch("https://api.github.com/users/alok-x0s1")
        .then(
            res => res.json()
        )
        .then(
            data => setData(data)
        )
    }, [])
    */

  return (
    <div className="flex justify-center items-center">
    <div className="card my-6">
      <div className="avatar">
        <img src={data.avatar_url} alt="User Avatar" id="image" />
      </div>
      {/* <div className="username">@{data.login}</div> */}
      <div className="name">{data.name}</div>
      <div className="follow-info">
        <div className="followers">
          <span>Followers</span>
          <br />
          <span id="followers">{data.followers}</span>
        </div>
        <div className="following">
          <span>Following</span>
          <br />
          <span id="following">{data.following}</span>
        </div>
      </div>
    </div>
    </div>
  );
};

export default Github;

export const githubInfoLoader = async () => {
    const res = await fetch("https://api.github.com/users/alok-x0s1")
    return res.json()
}