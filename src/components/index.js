import About from "./About/About";
import Contact from "./Contact/Contact";
import Home from "./Home/Home";
import User from "./User/User";
import Github from "./Github/Github";
import Signup from "./Signup/Signup";
import Login from "./Login/Login";

export {Home, About, Contact, User, Github, Signup, Login}